# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from django.forms.models import model_to_dict
from jobs.models import Job


class GetkaziPipeline(object):
    def process_item(self, item, spider):
        job, created = Job.objects.get_or_create(
            title=item["title"], company=item["company"]
        )

        if created:
            job.location = item["location"]
            job.category = item["category"]
            job.details = item["details"]
            job.end_date = item["end_date"]
            job.url = item["url"]
        else:
            print("Job listing already exists. Skipping")

        job.save()
        return job
