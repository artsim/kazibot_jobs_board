from datetime import datetime
import scrapy
from getkazi.items import JobItem


class JobSpider(scrapy.Spider):
    name = "ihub"

    start_urls = [
            'https://ihub.co.ke/jobs',
        ]

    def parse(self, response):
        # follow links to job pages
        for href in response.css('h3 a::attr(href)').extract():
            yield scrapy.Request(response.urljoin(href),
                                 callback=self.parse_job)

        # follow pagination links
        next_page = response.css('li.next a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_job(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        title = extract_with_css('h1::text')
        company = extract_with_css('li a::text')
        url = response.url
        location = extract_with_css('div.city-location::text')
        category = response.css('li a::text').extract()[1].strip().split()[0]
        details = response.css('div.job-content *::text').extract()
        text = ''.join(details).strip()
        end_date = extract_with_css('span.job-deadline::text')
        sdate = datetime.strptime(end_date, '%d %b, %Y').date()
        sfdate = sdate.strftime('%Y-%m-%d')

        return JobItem(title=title,
                       company=company,
                       location=location,
                       category=category,
                       details=text,
                       end_date=sfdate,
                       url=url)
