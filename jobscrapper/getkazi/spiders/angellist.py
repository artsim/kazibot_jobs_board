import scrapy
from getkazi.items import JobItem


class JobSpider(scrapy.Spider):
    name = "angels"

    start_urls = ['https://angel.co/kenya/jobs', ]

    def parse(self, response):
        # follow links to job pages
        for href in response.css('div.title a::attr(href)').extract():
            yield scrapy.Request(response.urljoin(href),
                                 callback=self.parse_job)

        # follow pagination links
        next_page = response.css('li.next a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_job(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        title = extract_with_css('h1::text')
        company = extract_with_css('li a::text')
        url = response.url
        location_details = response.css('div.high-concept *::text').extract()[0]
        dot = location_details.split()[1]
        location = location_details.split(dot)[0].strip()
        category = location_details.split(dot)[1].strip()
        details = response.css('div.job-description *::text').extract()
        text = ''.join(details).strip()

        return JobItem(title=title,
                       company=company,
                       location=location,
                       category=category,
                       details=text,
                       url=url)
