# Define here the models for your scraped #
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
from scrapy_djangoitem import DjangoItem
from jobs.models import Job


class JobItem(DjangoItem):
    django_model = Job
