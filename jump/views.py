from django.contrib.auth import authenticate
from django.http import HttpResponse

from .tasks import process_candidate_jobs


def get_candidate_jobs(request, user_id):
    from jobs.models import Candidate

    candidate = Candidate.objects.get(user_id=user_id)
    candidate_id = candidate.id
    process_candidate_jobs.apply_async(args=(candidate_id,))

    return HttpResponse("OK", status=200)
