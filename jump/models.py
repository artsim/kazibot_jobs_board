from django.db import models


class Skill(models.Model):
    name = models.CharField(max_length=250, default="", blank=True)
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class SkillSynonym(models.Model):
    name = models.CharField(max_length=250, default="", blank=True)
    skill = models.ForeignKey(Skill, related_name="synonyms", on_delete=models.CASCADE)
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    def __str__(self):
        return self.name
