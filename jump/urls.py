from django.urls import path

from .views import get_candidate_jobs

urlpatterns = [
    path("candidate/<int:user_id>/jobs", get_candidate_jobs, name="candidate_jobs")
]
