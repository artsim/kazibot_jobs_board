from django.contrib import admin

from .models import Skill, SkillSynonym


class SkillSynonymInline(admin.TabularInline):
    model = SkillSynonym


class SkillAdmin(admin.ModelAdmin):
    model = Skill
    inlines = [SkillSynonymInline]


admin.site.register(Skill, SkillAdmin)
