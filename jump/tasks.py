import math
import os
import time
from collections import Counter
from datetime import datetime, timedelta

import requests
import telegram
from celery.decorators import periodic_task, task
from celery.schedules import crontab
from celery.utils.log import get_task_logger
from django.utils import timezone
from flashtext import KeywordProcessor
from telegram.error import BadRequest

logger = get_task_logger(__name__)


PAT = (
    "EAASCNRlSpegBAOTzN04aOP8VUq1mxpVNw1wDbSc7h94WZBrVHvX7hdjpw76aqBj3mCf7"
    "RcGKPXRsLCkcX8PZBf05jnHCNtCAOZBUmVMkQglLMt0zr2YZCdaRSHNCO4Y1ReON92vJIk"
    "0Ep1s09zoZAZAwxEn0ovfOv4GNE8HXPfHfow4CK7flZCH"
)

# Telegram Production bot
TOKEN = "473681310:AAEd46KYO_rny-tjrrzltQpAbDP7Rv1I4Ys"

# Dev bot
# TOKEN = "590492585:AAFzKxnbc4PR4i58z7f6Q9Yu1tqU6Wf0QL4"


def read_skills_list(filename):
    with open(filename) as f:
        lines = [line.strip() for line in f]
    return lines


def similarity_score(l1, l2):
    c1, c2 = Counter(l1), Counter(l2)
    terms = set(c1).union(c2)
    dotprod = sum(c1.get(k, 0) * c2.get(k, 0) for k in terms)
    magA = math.sqrt(sum(c1.get(k, 0) ** 2 for k in terms))
    magB = math.sqrt(sum(c2.get(k, 0) ** 2 for k in terms))

    try:
        score = dotprod / (magA * magB)
    except ZeroDivisionError:
        score = 0

    return score


def score_candidate_to_job(candidate_id, job_id):
    """ Score candidate against Job"""
    keyword_processor = KeywordProcessor()
    from jobs.models import Candidate, Job, JobMatch

    # filepath = "/home/artsim/code/kazibot/board/jump/skills.txt"
    filepath = "/home/vyura/webapps/board/board/jump/skills.txt"
    skills = read_skills_list(filepath)

    for skill in skills:
        keyword_processor.add_keyword(skill)

    job = Job.objects.get(id=job_id)
    job_desc = job.details
    required_skills = keyword_processor.extract_keywords(job_desc)

    candidate = Candidate.objects.get(id=candidate_id)
    candidate_skills = list(candidate.skills.all().values_list("name", flat=True))
    score = similarity_score(candidate_skills, required_skills)
    return score


def match_candaidate_jobs(candidate_id):
    """Match candidate with jobs"""
    # candidate = Candidate.objects.get(id=candidate_id)
    # TODO: Get job category based on candidate role
    # candidate_role = candidate.role
    # candidate_category = candidate.role

    startdate = datetime.today().date()
    # category = "Developer"

    from jobs.models import Job

    jobs = Job.objects.filter(archived=False, end_date__gte=startdate)

    matching_jobs = []

    for job in jobs:
        score = score_candidate_to_job(candidate_id, job.id)

        if score >= 0.3:
            matching_jobs.append((score, job))

    return matching_jobs


def send_notification_to_user(candidate, job_match):
    title = job_match.job.title
    url = job_match.job.url
    message = (
        f"This is a job notification for a new job - {title} that matches "
        f"your profile. You can check it out here {url}"
    )
    channel = candidate.channel
    user_id = candidate.user_id
    send_message(channel, user_id, message)


def send_message(channel, user_id, message):
    def _send_telegram_notification(chat_id, text):
        """send notification to users via Telegram"""
        try:
            bot = telegram.Bot(token=TOKEN)
            bot.send_message(chat_id=chat_id, text=text)
            logger.info(f"send message to {chat_id} - {text}")
        except BadRequest:
            # TODO: Log using analytics as deleted user
            logger.info(f"Error occured sending message to {chat_id} - {text}")

    def _send_messenger_notification(recipient, text):
        """send notification to users via Messenger"""
        token = PAT
        data = {"recipient": {"id": recipient}, "message": {"text": text}}

        # Setup the query string with your PAGE TOKEN
        qs = "access_token=" + token
        # Send POST request to messenger
        try:
            logger.info(f"send message to {recipient} - {text}")
            resp = requests.post(
                "https://graph.facebook.com/me/messages?" + qs, json=data
            )
            return resp.content
        except Exception as e:
            logger.info(f"Error occured sending message to {recipient} - {text}")
            logger.info(e.msg)

    if channel == "Messenger":
        _send_messenger_notification(user_id, message)
        # _send_messenger_notification(user_id, message)
    elif channel == "Telegram":
        _send_telegram_notification(user_id, message)
        # _send_telegram_notification(user_id, message)


def process_matched_jobs(matching_jobs, cid):
    from jobs.models import Candidate

    candidate = Candidate.objects.get(id=cid)
    channel = candidate.channel
    user_id = candidate.user_id

    if matching_jobs:
        length = len(matching_jobs)

        if length > 1:
            job_text = "jobs"
        else:
            job_text = "job"

        message = f"I have found {length} {job_text} that match your profile"
        send_message(channel, user_id, message)
        job_list = []

        logger.info("Sending matched job listings to canidate")
        for score, job in matching_jobs:
            print(score, job.title)
            title = job.title
            company = job.company
            url = job.url

            message = (
                f"Title: {title} \nCompany: {company} \nYou can check it out here {url}"
            ) + "\n"
            job_list.append(message)

        jobs_list = "\n".join(job_list)
        send_message(channel, user_id, jobs_list)

    else:
        message = "I could not find any matching jobs at the moment"
        send_message(channel, user_id, message)
        message = (
            "Worry not. I will keep searching and update you once I find any matches"
        )
        send_message(channel, user_id, message)


@task(name="process_candidate")
def process_candidate_jobs(candidate_id):
    """Match a new candidate with existing jobs"""
    logger.info("Processing new candidate")
    logger.info("Matching candaite with existing jobs")

    matching_jobs = match_candaidate_jobs(candidate_id)
    process_matched_jobs(matching_jobs, candidate_id)


@task(name="match_jobs")
def match_jobs():
    """Match candidates with job listings"""
    from jobs.models import Candidate, Job, JobMatch

    startdate = datetime.today().date()
    jobs = Job.objects.filter(archived=False, end_date__gte=startdate)
    candidates = Candidate.objects.all()

    for job in jobs:
        for candidate in candidates:
            score = score_candidate_to_job(candidate.id, job.id)

            if score >= 0.3:
                job_match = JobMatch(candidate=candidate, job=job, score=score)
                job_match.save()


@task(name="send_notifications")
def send_notifications():
    """Send matched jobs notifications to candidates"""
    from jobs.models import Candidate, Job, JobMatch

    candidates = Candidate.objects.all()

    for candidate in candidates:
        send_notification = False
        frequency = candidate.frequency
        channel = candidate.channel
        user_id = candidate.user_id
        name = candidate.first_name
        last_notification_date = candidate.last_notification_date
        date_today = timezone.now()
        delta = date_today - last_notification_date

        if frequency == "Daily":
            if delta.days > 1:
                send_notification = True
        elif frequency == "Weekly":
            if delta.days > 7:
                send_notification = True

        if send_notification:
            jobmatches = JobMatch.objects.filter(candidate=candidate, notified=False)
            count = jobmatches.count()

            if count > 0:

                if count > 1:
                    job_text = "jobs"
                else:
                    job_text = "job"

                message = f"Hi {name}, I have found {count} new {job_text} that match your profile"
                send_message(channel, user_id, message)

                logger.info("Sending matched job listings to canidate")
                job_list = []

                for job_match in jobmatches:

                    title = job_match.job.title
                    company = job_match.job.company
                    url = job_match.job.url
                    message = (
                        f"Title: {title} \nCompany: {company} \nYou can check it out here {url}"
                    ) + "\n"
                    job_list.append(message)

                jobs_list = "\n".join(job_list)
                send_message(channel, user_id, jobs_list)

                for job_match in jobmatches:
                    job_match.notified = True
                    job_match.save()

                candidate.last_notification_date = timezone.now()
                candidate.save()


@task(name="send_notification")
def send_notification():
    from jobs.models import Candidate, JobMatch

    candidate = Candidate.objects.get(id=12)
    job_match = JobMatch.objects.get(id=2)
    send_notification_to_user(candidate, job_match)
