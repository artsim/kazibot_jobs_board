import math
import os
from collections import Counter
from datetime import datetime

import requests
from flashtext import KeywordProcessor


def archive_jobs():
    startdate = datetime.today().date()
    jobs_to_archive = Job.objects.filter(archived=False, end_date__lte=startdate)

    for job in jobs_to_archive:
        job.archived = True
        job.save()
