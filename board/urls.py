from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("jump/", include("jump.urls")),
    path("api/", include("api.urls")),
    path("admin/", admin.site.urls),
]
