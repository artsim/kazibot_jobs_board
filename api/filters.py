import django_filters

from jobs.models import Job


class JobFilter(django_filters.FilterSet):
    location = django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Job
        fields = ['location']
