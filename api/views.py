from api.serializers import (CandidateSerializer, JobSerializer,
                             MatchedJobSerializer)
from django_filters import rest_framework as filters
from jobs.models import Candidate, Job, JobMatch
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework_word_filter import FullWordSearchFilter

from .filters import JobFilter


class JobList(generics.ListAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    filter_backends = (filters.DjangoFilterBackend, FullWordSearchFilter)
    filter_class = JobFilter
    word_fields = ("title",)


@api_view(["GET"])
def detail(request, job_id):
    job = Job.objects.get(pk=job_id)
    serializer = JobSerializer(job)
    return Response(serializer.data)


@api_view(["GET"])
def candidates(request, job_id):
    job = Job.objects.get(pk=job_id)
    matched_candidates = job.matched.all()
    serializer = CandidateSerializer(matched_candidates)
    return Response(serializer.data)


@api_view(["GET"])
def candidate_jobs(request, user_id):
    candidate = Candidate.objects.get(user_id=user_id)
    jobs = candidate.matched_jobs.all()
    serializer = JobSerializer(jobs, many=True)
    return Response(serializer.data)


class CandidatesList(generics.ListCreateAPIView):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer


class CandidateDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer
    lookup_field = "user_id"


class MatchedJobList(generics.ListCreateAPIView):
    queryset = JobMatch.objects.all()
    serializer_class = MatchedJobSerializer


class MatchedJobDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = JobMatch.objects.all()
    serializer_class = MatchedJobSerializer
