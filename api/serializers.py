from jobs.models import Candidate, Job, JobMatch, Skill
from rest_framework import serializers


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ("name",)


class CandidateSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(many=True)

    class Meta:
        model = Candidate
        fields = (
            "user_id",
            "first_name",
            "last_name",
            "email",
            "role",
            "experience",
            "skills",
            "channel",
        )

    def create(self, validated_data):
        skills_data = validated_data.pop("skills")
        candidate = Candidate.objects.create(**validated_data)

        for skill_data in skills_data:
            Skill.objects.create(candidate=candidate, **skill_data)

        return candidate


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ("title", "company", "location", "description", "details", "url")


class MatchedJobSerializer(serializers.ModelSerializer):
    candidate = CandidateSerializer()
    job = JobSerializer()

    class Meta:
        model = JobMatch
        fields = ("candidate", "job", "viewed", "intrested")
