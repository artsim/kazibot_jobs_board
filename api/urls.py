from django.urls import path
from api.views import JobList, CandidatesList, CandidateDetail, detail, candidates, candidate_jobs, MatchedJobDetail, MatchedJobList
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('jobs/', JobList.as_view()),
    path('jobs/<int:job_id>/', detail, name='detail'),
    path('jobs/<int:job_id>/candidates', candidates, name='candidates'),
    path('candidate/<int:user_id>/jobs', candidate_jobs, name='candidate_jobs'),
    path('candidate/<int:user_id>', CandidateDetail.as_view()),
    path('candidates/', CandidatesList.as_view()),
    path('matchedjob/<int:pk>', MatchedJobDetail.as_view()),
    path('matchedjobs/', MatchedJobList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
