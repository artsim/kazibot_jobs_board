import threading
from datetime import datetime, timedelta

import redis

local_thread = threading.local()

# --- Systems related

SYSTEMS = {
    "dev": redis.StrictRedis(host="localhost", port=6379),
    "default": redis.StrictRedis(host="localhost", port=6379),
}


def get_redis():
    """
    Get a redis-py client instance with entry `system`.
    :param :system The name of the system, redis.StrictRedis or redis.Pipeline
        instance, extra systems can be setup via `setup_redis`
    """

    redis_pool = redis.ConnectionPool(host="127.0.0.1", port=6379, db=0)
    server = redis.Redis(connection_pool=redis_pool)
    return server


def mark_event(event_name, uuid):
    """
    Marks an event for hours, days, weeks and months.

    """
    _mark(event_name, uuid)


def _mark(event_name, uuid):
    now = datetime.utcnow()
    date = now.strftime("%Y-%m-%d")
    month = now.strftime("%Y-%m")
    year = now.strftime("%Y")

    day_key = f"{event_name}:{date}"
    month_key = f"{event_name}:{month}"
    year_key = f"{event_name}:{year}"

    event_keys = [day_key, month_key, year_key]

    client = get_redis()

    for event_key in event_keys:
        print(event_key)
        client.setbit(event_key, uuid, 1)


def event_active_user(uuid):
    """
    Marks active user event
    """

    mark_event("user:active", uuid)


def event_add_user(uuid):
    """
    Marks add user event
    """
    mark_event("user:add", uuid)


def event_create_profile(uuid):
    """
    Marks user created profile event
    """

    mark_event("user:profile_create", uuid)


def event_complete_profile(uuid):
    """
    Marks user finished profile creation event
    """
    mark_event("user:profile_complete", uuid)
