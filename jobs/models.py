import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from taggit.managers import TaggableManager

from jump.tasks import process_candidate_jobs

CHANNEL_CHOICES = (("Telegram", "Telegram"), ("Messenger", "Messenger"))

EXPERIENCE_CHOICES = (
    ("Beginner", "Beginner"),
    ("Intermediate", "Intermediate"),
    ("Advance", "Advance"),
)

JOB_CHOICES = (("Full Time", "Full Time"), ("Part Time", "Part Time"), ("Both", "Both"))

REQUIREMENT_CHOICES = (("Major", "Major"), ("Minor", "Minor"))

REQUIREMENT_TYPE = (("Skill", "Skill"), ("Experience", "Experience"))

FREQUENCY_CHOICES = (("Daily", "Daily"), ("Weekly", "Weekly"))


class Candidate(models.Model):
    first_name = models.CharField(max_length=256, blank=False)
    last_name = models.CharField(max_length=256, blank=False)
    user_id = models.CharField(max_length=256, blank=False, default=0)
    email = models.EmailField(blank=True)
    frequency = models.CharField(
        max_length=256, blank=False, choices=FREQUENCY_CHOICES, default="Daily"
    )
    channel = models.CharField(
        max_length=256, blank=False, choices=CHANNEL_CHOICES, default="Telegram"
    )
    role = models.CharField(max_length=256, blank=True)
    experience = models.IntegerField(blank=False)
    last_notification_date = models.DateTimeField(db_index=True, blank=True, null=True)
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name = "Candidate"
        verbose_name_plural = "Candidates"
        unique_together = ("user_id", "channel")


class Skill(models.Model):
    name = models.CharField(max_length=250, default="", blank=True)
    candidate = models.ForeignKey(
        Candidate, related_name="skills", on_delete=models.CASCADE
    )
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    def __str__(self):
        return self.name


class Job(models.Model):
    title = models.CharField(max_length=256, blank=False)
    company = models.CharField(max_length=256, blank=True)
    slug = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    location = models.CharField(max_length=256, blank=True)
    category = models.CharField(max_length=256, blank=True)
    job_type = models.CharField(max_length=256, blank=True)
    description = models.TextField(blank=True)
    details = models.TextField(blank=True)
    end_date = models.DateField(blank=True, null=True)
    tags = TaggableManager(blank=True)
    url = models.URLField(max_length=256, blank=True)
    verified = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    matched_candidates = models.ManyToManyField(Candidate, related_name="matched_jobs")
    intrested_candidates = models.ManyToManyField(
        Candidate, related_name="intrested_jobs"
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Job"
        verbose_name_plural = "Jobs"

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/j/%s" % str(self.slug)


class Requirement(models.Model):
    requirement_type = models.CharField(
        max_length=250, default="", choices=REQUIREMENT_TYPE, blank=True
    )
    name = models.CharField(max_length=250, default="", blank=True)
    job = models.ForeignKey(Job, related_name="requirements", on_delete=models.CASCADE)
    requirement_class = models.CharField(
        max_length=75, choices=REQUIREMENT_CHOICES, blank=True
    )
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    def __str__(self):
        return self.name


class JobMatch(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    score = models.FloatField()
    notified = models.BooleanField(default=False)
    viewed = models.BooleanField(default=False)
    intrested = models.BooleanField(default=False)
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True)

    def __str__(self):
        return self.job.title

    class Meta:
        verbose_name = "Job Match"
        verbose_name_plural = "Job Matches"
        unique_together = (("candidate", "job"),)


@receiver(post_save, sender=Candidate, dispatch_uid="process_candidate")
def candidate_post_save(sender, instance, created, *args, **kwargs):
    if created:
        process_candidate_jobs.apply_async(args=(instance.id,))
    else:
        print("not a new instance")


post_save.connect(
    candidate_post_save, sender=Candidate, dispatch_uid="process_candidate"
)
