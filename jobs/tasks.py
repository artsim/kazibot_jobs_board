from celery.decorators import task
from utils.processor import match_candaidate_jobs


@task(name="match_candaidate_task")
def match_candaidate_task(cid):
    """Match candidate with jobs task"""
    match_candaidate_jobs(cid)
