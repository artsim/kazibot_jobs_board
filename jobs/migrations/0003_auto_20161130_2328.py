# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0002_auto_20161130_2323'),
    ]

    operations = [
        migrations.RenameField(
            model_name='job',
            old_name='category',
            new_name='cat',
        ),
    ]
