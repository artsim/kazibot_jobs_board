# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_auto_20161130_2328'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='slug',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
