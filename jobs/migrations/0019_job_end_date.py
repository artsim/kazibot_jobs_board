# Generated by Django 2.0 on 2018-03-28 23:29

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0018_auto_20180322_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='end_date',
            field=models.DateField(blank=True, default=datetime.date(2018, 3, 28)),
            preserve_default=False,
        ),
    ]
