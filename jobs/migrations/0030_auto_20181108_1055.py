# Generated by Django 2.1.2 on 2018-11-08 10:55

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0029_auto_20180922_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobMatch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.FloatField()),
                ('notified', models.BooleanField(default=False)),
                ('viewed', models.BooleanField(default=False)),
                ('intrested', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified', models.DateTimeField(auto_now=True, db_index=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='matchedjob',
            name='candidate',
        ),
        migrations.RemoveField(
            model_name='matchedjob',
            name='job',
        ),
        migrations.AddField(
            model_name='candidate',
            name='frequency',
            field=models.CharField(choices=[('Daily', 'Daily'), ('Weekly', 'Weekly')], default='Daily', max_length=256),
        ),
        migrations.AddField(
            model_name='candidate',
            name='last_notification_date',
            field=models.DateTimeField(db_index=True, default=datetime.date(2018, 11, 8)),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='MatchedJob',
        ),
        migrations.AddField(
            model_name='jobmatch',
            name='candidate',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Candidate'),
        ),
        migrations.AddField(
            model_name='jobmatch',
            name='job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobs.Job'),
        ),
    ]
