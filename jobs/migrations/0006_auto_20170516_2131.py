# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0005_auto_20170116_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='company',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AddField(
            model_name='job',
            name='details',
            field=models.TextField(blank=True),
        ),
    ]
