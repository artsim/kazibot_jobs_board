# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0004_job_slug'),
    ]

    operations = [
        migrations.RenameField(
            model_name='job',
            old_name='cat',
            new_name='category',
        ),
        migrations.RemoveField(
            model_name='job',
            name='details',
        ),
        migrations.AddField(
            model_name='job',
            name='url',
            field=models.URLField(max_length=256, blank=True),
        ),
    ]
