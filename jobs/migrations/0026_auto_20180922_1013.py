# Generated by Django 2.1.1 on 2018-09-22 10:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0025_auto_20180910_1435'),
    ]

    operations = [
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=250)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified', models.DateTimeField(auto_now=True, db_index=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='candidate',
            name='job_type',
        ),
        migrations.RemoveField(
            model_name='candidate',
            name='skills',
        ),
        migrations.AddField(
            model_name='candidate',
            name='email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AddField(
            model_name='skill',
            name='job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='skills', to='jobs.Candidate'),
        ),
    ]
