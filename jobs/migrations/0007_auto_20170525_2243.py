# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0006_auto_20170516_2131'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='archived',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='job',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 25, 22, 43, 32, 565998, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='job',
            name='verified',
            field=models.BooleanField(default=False),
        ),
    ]
