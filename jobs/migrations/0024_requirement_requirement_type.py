# Generated by Django 2.1.1 on 2018-09-09 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0023_auto_20180909_1330'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirement',
            name='requirement_type',
            field=models.CharField(blank=True, choices=[('Skill', 'Skill'), ('Experience', 'Experience')], default='', max_length=250),
        ),
    ]
