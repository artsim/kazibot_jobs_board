from django.contrib import admin

from .models import Candidate, Job, JobMatch, Requirement, Skill


class SkillInline(admin.TabularInline):
    model = Skill


class CandidateAdmin(admin.ModelAdmin):
    model = Candidate
    inlines = [SkillInline]


class RequirementInline(admin.TabularInline):
    model = Requirement


class JobAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "company",
        "location",
        "category",
        "job_type",
        "created_at",
        "verified",
        "archived",
    ]
    inlines = [RequirementInline]


admin.site.register(Job, JobAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(JobMatch)
